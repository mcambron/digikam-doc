# Spanish translations for docs_digikam_org_right_sidebar___captions_view.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Automatically generated, 2022.
# Eloy Cuadra <ecuadra@eloihr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: docs_digikam_org_right_sidebar___captions_view\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-01-09 12:18+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: ../../right_sidebar/captions_view.rst:1
msgid "digiKam Right Sidebar Captions View"
msgstr "La vista de pies de foto de la barra lateral derecha de digiKam"

#: ../../right_sidebar/captions_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, description, captions, title, author, labels, rating, date, "
"tags, template"
msgstr ""
"digiKam, documentación, manual del usuario, gestión de fotos, gestión "
"fotográfica, código abierto, libre, gratis, aprender, fácil, descripción, "
"pie de foto, título, autor, emblemas, puntuación, valoración, fecha, "
"etiquetas, plantilla"

#: ../../right_sidebar/captions_view.rst:14
msgid "Captions View"
msgstr "Vista de pies de foto"

#: ../../right_sidebar/captions_view.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../right_sidebar/captions_view.rst:19
msgid "Overview"
msgstr "Resumen"

#: ../../right_sidebar/captions_view.rst:21
msgid ""
"This sidebar tab serves to apply and edit image attributes like captions, "
"rating, date and tags. The attributes are stored in the associated database, "
"in the IPTC, XMP, and Exif data fields and become part of the image. All "
"attributes are accessible in one sidebar view as shown in the screenshot "
"below. During image reading the order of priority is ``a)`` database ``b)`` "
"XMP/IPTC and ``c)`` Exif. So if there is a discrepancy between any of the "
"three, this priority will take effect and a synchronization will take place. "
"This sidebar has a first-previous-next-last arrow navigator field on top if "
"shown in the main application."
msgstr ""

#: ../../right_sidebar/captions_view.rst:27
msgid "The Captions View From Right Sidebar Displaying Description Information"
msgstr ""

#: ../../right_sidebar/captions_view.rst:32
msgid "Comment Editors"
msgstr ""

#: ../../right_sidebar/captions_view.rst:34
msgid ""
"The descriptions view can be used to type or paste in a title and/or a "
"captions of unlimited size (see note below). The text is UTF-8 compatible, "
"meaning that all special characters are allowed. The captions are copied to "
"Exif, IPTC, and XMP fields to be used by other applications."
msgstr ""

#: ../../right_sidebar/captions_view.rst:38
msgid ""
"IPTC data only supports ASCII characters and is limited to 2000 characters "
"(old American norm). All texts will be truncated after 2000 chars, and "
"special characters will be malformed. If you intend to use the IPTC caption "
"field in other applications you should be compliant with these restrictions."
msgstr ""

#: ../../right_sidebar/captions_view.rst:40
msgid "Title and caption editors are powerful tool which supports:"
msgstr ""

#: ../../right_sidebar/captions_view.rst:42
msgid "Multiple alternative language strings."
msgstr ""

#: ../../right_sidebar/captions_view.rst:43
msgid "Translate strings online to another language."
msgstr ""

#: ../../right_sidebar/captions_view.rst:44
msgid "Author strings definition."
msgstr ""

#: ../../right_sidebar/captions_view.rst:46
msgid ""
"The default language from an alternative strings stack is **x-default** and "
"must be written by preference in English. If one string must be entered in "
"the stack, the x-default must be present in prior."
msgstr ""

#: ../../right_sidebar/captions_view.rst:48
msgid ""
"After commenting, either choose the **Apply** button or go straight to the "
"next image, the descriptions will be saved."
msgstr ""

#: ../../right_sidebar/captions_view.rst:50
msgid ""
"Next to the Apply button there is the **More** button. From it you can "
"either choose to read metadata from the selected file to the database, or "
"the other way around, to write metadata to the files (the latter take place "
"anyway if you chose a metadata setting so that all metadata is always saved "
"to the images)."
msgstr ""

#: ../../right_sidebar/captions_view.rst:55
msgid "Date and Time"
msgstr "Fecha y hora"

#: ../../right_sidebar/captions_view.rst:57
msgid ""
"In the Date and Time section, which reflects the time of taking the "
"photograph, you can change all values. From the date combo-box a calendar "
"opens, and the time setting spin-box can also be written by directly typing "
"the time. The dating is copied to the Exif **Date and Time** field. If you "
"need to change a number of images for their creating time & date, there is a "
"more comfortable method available in **Batch Queue Manager** or from :"
"menuselection:`Item --> Adjust time & date...` menu entry in **Main "
"Window**. Select the images to be changed in the main view and call the tool."
msgstr ""

#: ../../right_sidebar/captions_view.rst:62
msgid "Labels"
msgstr "Etiquetas"

#: ../../right_sidebar/captions_view.rst:64
msgid ""
"The Rating section displays a 0...5 star rating scheme that can be used in "
"searches and sort orders. It can be applied by a single mouse click to the 5 "
"stars in the sidebar or with a keyboard shortcut :kbd:`Ctrl+0...5`. The "
"rating from the sidebar is always applied to one image at a time. To rate a "
"number of images, select them and pop-up the context menu (click with the "
"right mouse button) to apply a common rating."
msgstr ""

#: ../../right_sidebar/captions_view.rst:66
msgid ""
"The labels view allow to assign also the Color and the Pick tags that you "
"can use in your workflow to classify items."
msgstr ""

#: ../../right_sidebar/captions_view.rst:68
msgid ""
"The rating is then transcribed into the IPTC *urgency* data field. The "
"transcoding follows the scheme in this table:"
msgstr ""

#: ../../right_sidebar/captions_view.rst:71
msgid "digiKam Rating"
msgstr ""

#: ../../right_sidebar/captions_view.rst:71
msgid "IPTC Urgency"
msgstr ""

#: ../../right_sidebar/captions_view.rst:73
msgid "no star"
msgstr "ninguna estrella"

#: ../../right_sidebar/captions_view.rst:73
msgid "8"
msgstr "8"

#: ../../right_sidebar/captions_view.rst:74
#: ../../right_sidebar/captions_view.rst:75
msgid "1 star"
msgstr "1 estrella"

#: ../../right_sidebar/captions_view.rst:74
msgid "7"
msgstr "7"

#: ../../right_sidebar/captions_view.rst:75
msgid "6"
msgstr "6"

#: ../../right_sidebar/captions_view.rst:76
msgid "2 stars"
msgstr "2 estrellas"

#: ../../right_sidebar/captions_view.rst:76
msgid "5"
msgstr "5"

#: ../../right_sidebar/captions_view.rst:77
msgid "3 stars"
msgstr "3 estrellas"

#: ../../right_sidebar/captions_view.rst:77
msgid "4"
msgstr "4"

#: ../../right_sidebar/captions_view.rst:78
#: ../../right_sidebar/captions_view.rst:79
msgid "4 stars"
msgstr "4 estrellas"

#: ../../right_sidebar/captions_view.rst:78
msgid "3"
msgstr "3"

#: ../../right_sidebar/captions_view.rst:79
msgid "2"
msgstr "2"

#: ../../right_sidebar/captions_view.rst:80
msgid "5 stars"
msgstr "5 estrellas"

#: ../../right_sidebar/captions_view.rst:80
msgid "1"
msgstr "1"

#: ../../right_sidebar/captions_view.rst:86
msgid "Tags Tree"
msgstr "Árbol de etiquetas"

#: ../../right_sidebar/captions_view.rst:88
msgid ""
"The tag view shows an adaptive filter tag search box, the tag tree and a "
"combo-box containing the tags previously applied in this digiKam session."
msgstr ""

#: ../../right_sidebar/captions_view.rst:94
msgid "The Metadata View From Right Sidebar Displaying Tags Information"
msgstr ""

#: ../../right_sidebar/captions_view.rst:96
msgid ""
"The tag tree will be adapted dynamically as a function of the search word as "
"you type into the box. So it is easy to quickly reduce the number of "
"possibilities when searching for a tag. Of course, this feature is only "
"useful if you have many tags."
msgstr ""

#: ../../right_sidebar/captions_view.rst:98
msgid ""
"The combo-box at the bottom is another ergonomic feature for easy tagging of "
"an image series. As you apply different tags they will be memorized in this "
"box for quick access."
msgstr ""

#: ../../right_sidebar/captions_view.rst:100
msgid ""
"Otherwise tags are simply applied by checking the respective boxes in the "
"tree. All tags of an image are transcribed into the XMP/IPTC *keywords* data "
"field."
msgstr ""

#: ../../right_sidebar/captions_view.rst:104
msgid ""
"In case you have selected a number of images in the Icon-View and you check "
"a tag in the tag tree, this one is only applied to the highlighted images, "
"and not to the whole Album contents."
msgstr ""

#: ../../right_sidebar/captions_view.rst:109
msgid "Information View"
msgstr "Visor de información"

#: ../../right_sidebar/captions_view.rst:115
msgid "The Metadata View From Right Sidebar Displaying Template Information"
msgstr ""

#: ../../right_sidebar/captions_view.rst:117
msgid ""
"The Information view allows to assign a template of textuals information to "
"items in one pass. This information can be populated in Setup/Template "
"dialog. They contain all strings describing the contents, the scene, the "
"authors, the rights, the place etc."
msgstr ""

#: ../../right_sidebar/captions_view.rst:119
msgid ""
"For more details about Template, see the description of the :ref:`Template "
"Settings <templates_settings>` section."
msgstr ""
