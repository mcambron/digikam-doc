# Spanish translations for docs_digikam_org_slideshow_tools___opengl_viewer.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Automatically generated, 2022.
# Eloy Cuadra <ecuadra@eloihr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: docs_digikam_org_slideshow_tools___opengl_viewer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-03-26 20:08+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.80\n"

#: ../../slideshow_tools/opengl_viewer.rst:1
msgid "Using digiKam OpenGL Viewer"
msgstr "Uso del visor OpenGL de digiKam"

#: ../../slideshow_tools/opengl_viewer.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, slide, opengl"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:14
msgid "OpenGL Viewer"
msgstr "Visor OpenGL"

#: ../../slideshow_tools/opengl_viewer.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../slideshow_tools/opengl_viewer.rst:18
msgid ""
"This tool preview a series of items using OpenGL hardware to speed-up "
"rendering. There is no configuration dialog. Calling this tool from :"
"menuselection:`View --> OpenGL Image Viewer` will show items in full-screen "
"mode."
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:22
msgid ""
"This tool does not include an OSD (On Screen Display). Navigating between "
"items is done with keyboard and mouse."
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:29
msgid "Screencast of the OpenGL Viewer"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:31
msgid ""
"The usage from Keyboard and mouse to quickly navigate between items is "
"listen below:"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:33
msgid "Item Access"
msgstr "Acceso del elemento"

#: ../../slideshow_tools/opengl_viewer.rst:36
msgid ":kbd:`Up` key :kbd:`PgUp` key :kbd:`Left` key Mouse wheel up"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:39
msgid "Previous Item:"
msgstr "Elemento anterior:"

#: ../../slideshow_tools/opengl_viewer.rst:42
msgid ":kbd:`Down` key :kbd:`PgDown` key :kbd:`Right` key Mouse wheel down"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:45
msgid "Next Item:"
msgstr "Elemento siguiente:"

#: ../../slideshow_tools/opengl_viewer.rst:48
msgid "Quit:"
msgstr "Salir:"

#: ../../slideshow_tools/opengl_viewer.rst:48
msgid ":kbd:`Esc` key"
msgstr "Tecla :kbd:`Esc`"

#: ../../slideshow_tools/opengl_viewer.rst:50
msgid "Item Display"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:53
msgid "Toggle fullscreen to normal:"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:53
msgid ":kbd:`f` key"
msgstr "Tecla :kbd:`f`"

#: ../../slideshow_tools/opengl_viewer.rst:56
msgid "Toggle scroll-wheel action:"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:56
msgid ":kbd:`c` key (either zoom or change image)"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:59
msgid "Rotation:"
msgstr "Rotación:"

#: ../../slideshow_tools/opengl_viewer.rst:59
msgid ":kbd:`r` key"
msgstr "Tecla :kbd:`r`"

#: ../../slideshow_tools/opengl_viewer.rst:62
msgid "Reset view:"
msgstr "Reiniciar vista:"

#: ../../slideshow_tools/opengl_viewer.rst:62
msgid "double click"
msgstr "doble clic"

#: ../../slideshow_tools/opengl_viewer.rst:65
msgid "Original size:"
msgstr "Tamaño original:"

#: ../../slideshow_tools/opengl_viewer.rst:65
msgid ":kbd:`o` key"
msgstr "Tecla :kbd:`o`"

#: ../../slideshow_tools/opengl_viewer.rst:68
msgid ""
"Move mouse in up-down-direction while pressing the right mouse button :kbd:"
"`c` key and use the scroll-wheel :kbd:`+` and :kbd:`-` keys :kbd:`ctrl` + "
"scrollwheel"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:71
msgid "Zooming:"
msgstr "Ampliación:"

#: ../../slideshow_tools/opengl_viewer.rst:74
msgid "Panning:"
msgstr "Panorama:"

#: ../../slideshow_tools/opengl_viewer.rst:74
msgid "Move mouse while pressing the left button"
msgstr ""

#: ../../slideshow_tools/opengl_viewer.rst:76
msgid "Others"
msgstr "Otros"

#: ../../slideshow_tools/opengl_viewer.rst:78
msgid "Show help dialog:"
msgstr "Mostrar el diálogo de ayuda:"

#: ../../slideshow_tools/opengl_viewer.rst:79
msgid ":kbd:`F1` key"
msgstr "Tecla :kbd:`F1`"

#~ msgid "f key"
#~ msgstr "Tecla f"

#~ msgid "r key"
#~ msgstr "Tecla r"

#~ msgid "o key"
#~ msgstr "Tecla o"
