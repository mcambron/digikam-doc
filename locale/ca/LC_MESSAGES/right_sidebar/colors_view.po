# Translation of docs_digikam_org_right_sidebar___colors_view.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-04-16 17:01+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.07.70\n"

#: ../../right_sidebar/colors_view.rst:1
msgid "digiKam Right Sidebar Colors View"
msgstr "Vista de colors s en la barra lateral dreta del digiKam"

#: ../../right_sidebar/colors_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, colors, histogram, icc, profile"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, colors, histograma, icc, perfil"

#: ../../right_sidebar/colors_view.rst:14
msgid "Colors View"
msgstr "La vista Colors"

#: ../../right_sidebar/colors_view.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../right_sidebar/colors_view.rst:18
msgid ""
"The colors sidebar has two sub tabs **Histogram** and **ICC Profile**. Here "
"are more details about :ref:`Color Management <color_management>`"
msgstr ""
"La barra lateral de colors té dues subpestanyes, **Histograma** i **Perfil "
"ICC**. Aquí hi ha més detalls sobre la :ref:`gestió del color "
"<color_management>`"

#: ../../right_sidebar/colors_view.rst:21
msgid "Histogram Viewer"
msgstr "Visualitzador de l'histograma"

#: ../../right_sidebar/colors_view.rst:23
msgid ""
"The histogram for an image shows the amount of each color that is present "
"and their different amplitudes within the image. If your photograph has a "
"color cast you might be able to see what is wrong by looking at the "
"histogram."
msgstr ""
"L'histograma d'una imatge mostra la quantitat de cada color que hi ha "
"present i les amplituds diferents que tenen dins de la imatge. Si la vostra "
"fotografia té un repartiment de colors, és possible que pugueu veure què "
"està mal mirant l'histograma."

#: ../../right_sidebar/colors_view.rst:25
msgid ""
"The Histogram Viewer shows the statistical distribution of color values in "
"the current image. It is purely informational: nothing you do with it will "
"cause any change to the image. If you want to perform a histogram based "
"color correction, use for example Color Balance, Levels Adjust or Curves "
"Adjust in the Image Editor."
msgstr ""
"El visualitzador de l'histograma mostra la distribució estadística dels "
"valors de color en la imatge actual. És purament informatiu: res del que feu "
"causarà cap canvi a la imatge. Si voleu fer una correcció de color basada en "
"histograma, utilitzeu, per exemple, Equilibri de color, Ajust dels nivells o "
"Ajust de les corbes a l'editor d'imatges."

#: ../../right_sidebar/colors_view.rst:31
msgid "The Color View From Right Sidebar Displaying Histogram Information"
msgstr ""
"La vista de colors des de la barra lateral dreta mostrant la informació de "
"l'histograma"

#: ../../right_sidebar/colors_view.rst:33
msgid ""
"An image can be decomposed into **Red**, **Green** and **Blue** color "
"channels. **Alpha** channel is a Layer in the image that supports "
"transparency (like PNG or GIF images). Each channel supports a range of "
"intensity levels from 0 to 255 (integer valued). Thus, a black pixel is "
"encoded by 0 on all color channels; a white pixel by 255 on all color "
"channels. A transparent pixel is encoded by 0 on the alpha channel; an "
"opaque pixel by 255."
msgstr ""
"Una imatge es pot descompondre en els canals de color **Vermell**, **Verd** "
"i **Blau**. El canal alfa és una capa a la imatge admet transparència (com "
"les imatges PNG o GIF). Cada canal admet un interval de nivells d'intensitat "
"de 0 fins a 255 (valors sencers). Per tant, un píxel negre es codificarà amb "
"0 en tots els canals de color. Un píxel blanc amb 255 en tots els canals de "
"color. Un píxel transparent amb 0 en el canal alfa. Un píxel opac amb 255."

#: ../../right_sidebar/colors_view.rst:35
msgid "The Histogram Viewer allows you to view each channel separately:"
msgstr "El visualitzador de l'histograma permet veure cada canal per separat:"

#: ../../right_sidebar/colors_view.rst:37
msgid "**Luminosity**: shows the distribution of brightness values."
msgstr "**Lluminositat**: mostra la distribució dels valors de la brillantor."

#: ../../right_sidebar/colors_view.rst:39
msgid ""
"**Red**, **Green**, **Blue**: show the distribution of intensity levels for "
"the Red, Green, or Blue channels respectively."
msgstr ""
"**Vermell**, **Verd** i **Blau**: mostra la distribució dels nivells "
"d'intensitat per als canals Vermell, Verd o Blau respectivament."

#: ../../right_sidebar/colors_view.rst:41
msgid ""
"**Alpha**: shows the distribution of opacity levels. If the layer is "
"completely opaque or completely transparent, the histogram will consist of a "
"single bar on the left or right edge."
msgstr ""
"**Alfa**: mostra la distribució dels nivells d'opacitat. Si la capa és "
"completament opaca o completament transparent, l'histograma consistirà en "
"una sola barra a la vora esquerra o dreta."

#: ../../right_sidebar/colors_view.rst:43
msgid ""
"**Colors**: shows the **Red**, **Green**, and **Blue** histograms "
"superposed, so that you can see all of the color distribution information in "
"a single view."
msgstr ""
"**Colors**: mostra els histogrames **Vermell**, **Verd** i **Blau** "
"superposats, de manera que es pugui veure tota la informació de la "
"distribució de colors en una sola vista."

#: ../../right_sidebar/colors_view.rst:45
msgid ""
"With the **Scale** option you can determine whether the histogram will be "
"displayed using a linear or logarithmic Y axis. For images taken with a "
"digital camera, the **Linear** mode is usually the most useful. However, for "
"images that contain substantial areas of constant color a **Linear** "
"histogram will often be dominated by a single bar. In this case a "
"**Logarithmic** histogram will be more useful."
msgstr ""
"Amb l'opció **Escala**, podreu determinar si l'histograma es mostrarà "
"utilitzant un eix Y lineal o logarítmic. Per a les imatges preses amb una "
"càmera digital, el mode **Lineal** sol ser el més útil. No obstant això, per "
"a les imatges que contenen àrees substancials d'un color constant, un "
"histograma lineal sovint estarà dominat per una sola barra. En aquest cas, "
"un histograma logarítmic serà més útil."

#: ../../right_sidebar/colors_view.rst:47
msgid ""
"You can restrict the analysis of the **Statistics** field shown at the "
"bottom of the dialog to a limited range of values if you wish. You can set "
"the range in one of two ways:"
msgstr ""
"Si voleu, podeu restringir l'anàlisi del camp **Estadístiques** que es "
"mostra a la part inferior del diàleg a un interval limitat de valors. Podeu "
"establir l'interval de dues maneres:"

#: ../../right_sidebar/colors_view.rst:49
msgid ""
"Click and drag the pointer across the histogram display area, from the "
"lowest level to the highest level of the range you want."
msgstr ""
"Feu clic i arrossegueu el punter per l'àrea de visualització de "
"l'histograma, des del nivell més baix fins al nivell més alt de l'interval "
"que voleu."

#: ../../right_sidebar/colors_view.rst:51
msgid ""
"Use the spin button entries below the histogram area. Left entry is bottom "
"of range and right entry is top of range."
msgstr ""
"Utilitzeu les entrades del botó de gir a sota de l'àrea de l'histograma. "
"L'entrada esquerra és la part inferior de l'interval i l'entrada dreta és la "
"part superior."

#: ../../right_sidebar/colors_view.rst:53
msgid ""
"The statistics shown at the bottom of the Histogram Viewer describe the "
"distribution of channel values, restricted to the selected range. These are:"
msgstr ""
"Les estadístiques que es mostren a la part inferior del visualitzador de "
"l'histograma descriuen la distribució dels valors dels canals, restringida a "
"l'interval seleccionat. Aquestes són:"

#: ../../right_sidebar/colors_view.rst:55
msgid "The number of pixels in the image."
msgstr "El nombre de píxels a la imatge."

#: ../../right_sidebar/colors_view.rst:57
msgid "The number whose values fall within the selected range."
msgstr ""
"El nombre els valors del qual es troben dins de l'interval seleccionat."

#: ../../right_sidebar/colors_view.rst:59
msgid "The mean."
msgstr "El significat."

#: ../../right_sidebar/colors_view.rst:61
msgid "The standard deviation."
msgstr "La desviació estàndard."

#: ../../right_sidebar/colors_view.rst:63
msgid "The median of the selected histogram portion."
msgstr "La mediana de la part seleccionada a l'histograma."

#: ../../right_sidebar/colors_view.rst:65
msgid "The percentage whose values fall within the selected range."
msgstr ""
"El percentatge els valors del qual es troben dins de l'interval seleccionat."

#: ../../right_sidebar/colors_view.rst:67
msgid "The color depth of the image."
msgstr "La profunditat de color de la imatge."

#: ../../right_sidebar/colors_view.rst:69
msgid "Alpha channel in the image."
msgstr "Canal alfa a la imatge."

#: ../../right_sidebar/colors_view.rst:71
msgid ""
"The source of the histogram, either **Full Image** or **Image Region** if "
"you have selected an area of the image loaded in the Editor."
msgstr ""
"La font de l'histograma, ja sigui **Imatge completa** o **Regió de la "
"imatge** si heu seleccionat a l'editor una àrea de la imatge carregada."

#: ../../right_sidebar/colors_view.rst:74
msgid "How To Use an Histogram"
msgstr "Com usar un histograma"

#: ../../right_sidebar/colors_view.rst:76
msgid ""
"Histograms are a graphical means to assess the accuracy of an image shown on "
"the screen. The graph represents the 3 regions of the photograph brightness:"
msgstr ""
"Els histogrames són un mitjà gràfic per a avaluar la precisió d'una imatge "
"que es mostra a la pantalla. El gràfic representa les 3 regions de la "
"brillantor en la fotografia:"

#: ../../right_sidebar/colors_view.rst:78
msgid ": the shadows-tone on the left."
msgstr ": el to de les ombres a l'esquerra."

#: ../../right_sidebar/colors_view.rst:80
msgid ": the middle-tone in the middle."
msgstr ": el to mitjà al mig."

#: ../../right_sidebar/colors_view.rst:82
msgid ": the highlights-tone on the right."
msgstr ": el to de la llum intensa a la dreta."

#: ../../right_sidebar/colors_view.rst:88
msgid "An Image Histogram in All Colors Mode"
msgstr "Un histograma d'imatge en el mode Tots els colors"

#: ../../right_sidebar/colors_view.rst:90
msgid ""
"The distribution of the graph, where the spikes and bulges are clustered, "
"indicates whether the image is too dark, too bright, or well-balanced."
msgstr ""
"La distribució del gràfic, on s'agrupen els pics i les protuberàncies, "
"indica si la imatge és massa fosca, massa brillant o bé que està equilibrada."

#: ../../right_sidebar/colors_view.rst:92
msgid ""
"With an under exposed photograph, the histogram will have a distribution of "
"brightness that tends to be mostly on the left of the graph."
msgstr ""
"Amb una fotografia subexposada, l'histograma tindrà una distribució de la "
"brillantor que tendirà a estar principalment a l'esquerra del gràfic."

#: ../../right_sidebar/colors_view.rst:98
msgid "An Under Exposed Photograph"
msgstr "Una fotografia subexposada"

#: ../../right_sidebar/colors_view.rst:100
msgid ""
"With a correctly exposed photograph, the histogram will have a distribution "
"of brightness that will be most prominent near the center part of the graph."
msgstr ""
"Amb una fotografia exposada correctament, l'histograma tindrà una "
"distribució de la brillantor que serà més prominent a prop de la part "
"central del gràfic."

#: ../../right_sidebar/colors_view.rst:106
msgid "A Correctly Exposed Photograph"
msgstr "Una fotografia exposada correctament"

#: ../../right_sidebar/colors_view.rst:108
msgid ""
"With an over exposed photograph, the histogram will have the bulge showing "
"the brightness distributed mostly towards the right of the graph."
msgstr ""
"Amb una fotografia sobreexposada, l'histograma tindrà la protuberància que "
"mostra la brillantor distribuïda principalment cap a la dreta del gràfic."

#: ../../right_sidebar/colors_view.rst:114
msgid "An Over Exposed Photograph"
msgstr "Una fotografia sobreexposada"

#: ../../right_sidebar/colors_view.rst:116
msgid ""
"Not all photographs have to exhibit this bulge in the center part of their "
"histogram. Much depends on the subject of the photograph. In some cases, it "
"might be appropriate for the histogram to show a peak at one end or the "
"other, or both."
msgstr ""
"No totes les fotografies han d'exhibir aquesta protuberància a la part "
"central del seu histograma. Dependrà molt del tema de la fotografia. En "
"alguns casos, podria ser apropiat que l'histograma mostri un pic en un "
"extrem o en l'altre, o en ambdós."

#: ../../right_sidebar/colors_view.rst:118
msgid ""
"The histogram is a reliable way of deciding whether or not a photograph is "
"correctly exposed. Should the histogram show an over or under exposure, an :"
"ref:`Exposure Correction Tool <color_bcg>` should be used to fix the "
"photograph."
msgstr ""
"L'histograma és una manera fiable de decidir si una fotografia està exposada "
"correctament o no. Si l'histograma mostra una sobreexposició o subexposició, "
"cal utilitzar una :ref:`Eina de correcció de l'exposició <color_bcg>` per a "
"esmenar la fotografia."

#: ../../right_sidebar/colors_view.rst:121
msgid "ICC Profile Viewer"
msgstr "Visualitzador del perfil ICC"

# skip-rule: t-acc_obe
#: ../../right_sidebar/colors_view.rst:123
msgid ""
"An ICC profile is a set of data that characterizes a color input or output "
"device, or a color space, according to standards promulgated by the "
"`International Color Consortium <https://en.wikipedia.org/wiki/"
"International_Color_Consortium>`_. Profiles describe the color attributes of "
"a particular device or viewing requirement by defining a mapping between the "
"device source or target color space and a profile connection space. Mappings "
"may be specified using tables, to which interpolation is applied, or through "
"a series of parameters for transformations used in Color Management."
msgstr ""
"Un perfil ICC és un conjunt de dades que caracteritza un dispositiu "
"d'entrada o sortida del color, o un espai de color, segons els estàndards "
"promulgats pel `Consorci internacional del color <https://en.wikipedia.org/"
"wiki/International_Color_Consortium>`_. Els perfils descriuen els atributs "
"de color d'un dispositiu en concret o un requisit de visualització "
"mitjançant la definició d'una assignació entre l'espai de color de "
"destinació o d'origen del dispositiu i un espai de connexió amb el perfil. "
"Les assignacions es poden especificar mitjançant taules a les quals "
"s'aplicarà la interpolació o mitjançant una sèrie de paràmetres per a les "
"transformacions utilitzades en la gestió del color."

#: ../../right_sidebar/colors_view.rst:125
msgid ""
"Every files used to store captured image can be profiled. Camera "
"manufacturers provide profiles for their products, and store them in image "
"file as extra metadata. This ICC Profile viewer allows to display the "
"textual information and a flat graph of color space."
msgstr ""
"Tots els fitxers utilitzats per a emmagatzemar la imatge capturada se'ls pot "
"crear un perfil. Els fabricants de càmeres proporcionen perfils per als seus "
"productes i els emmagatzemen en un fitxer d'imatge com a metadades "
"addicionals. Aquest visualitzador del perfil ICC permet mostrar la "
"informació textual i un gràfic pla de l'espai de color."

#: ../../right_sidebar/colors_view.rst:132
msgid "The Color View From Right Sidebar Displaying Color Profile Information"
msgstr ""
"La vista de colors des de la barra lateral dreta mostrant la informació del "
"perfil de color"
