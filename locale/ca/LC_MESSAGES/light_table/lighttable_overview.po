# Translation of docs_digikam_org_light_table___lighttable_overview.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-22 00:43+0000\n"
"PO-Revision-Date: 2023-04-22 11:56+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.07.70\n"

#: ../../light_table/lighttable_overview.rst:1
msgid "Overview to digiKam Light Table"
msgstr "Vista general de la taula de llum del digiKam"

#: ../../light_table/lighttable_overview.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, light table, synchronize, by-pair, compare, preview, canvas"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, ajuda, aprenentatge, fàcil, taula de llum, sincronitzar, per "
"parella, comparar, vista prèvia, llenç"

#: ../../light_table/lighttable_overview.rst:14
msgid "Overview"
msgstr "Vista general"

#: ../../light_table/lighttable_overview.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../light_table/lighttable_overview.rst:22
msgid "The Light Table Comparing Side By Side Two RAW Files"
msgstr "La Taula de llum comparant costat a costat dos fitxers RAW"

#: ../../light_table/lighttable_overview.rst:24
msgid ""
"digiKam features a light table in a separate window to easily compare "
"images. It works with all supported image formats including RAW files."
msgstr ""
"El digiKam compta amb petits detalls com una taula de llum en una finestra "
"separada per a comparar les imatges amb facilitat. Funciona amb tots els "
"formats d'imatge admesos, inclosos els fitxers RAW."

#: ../../light_table/lighttable_overview.rst:30
msgid ""
"The Main Icon-View Context Menu To Place Items To Compare On Light Table"
msgstr ""
"El menú contextual de la vista d'icones principal quan es col·loquen "
"elements per a comparar a la Taula de llum"

#: ../../light_table/lighttable_overview.rst:32
msgid ""
"Select one or several images in any view from the main window, call **Place "
"onto Light Table** :kbd:`Ctrl+L` from the context menu. The selection will "
"be added to the light table, and its separate window will open. When you are "
"back to the digiKam main window you can quickly access the light table with :"
"menuselection:`Tools --> Light Table` :kbd:`L`."
msgstr ""
"Seleccioneu una o diverses imatges en qualsevol vista des de la finestra "
"principal, crideu **Col·loca sobre la taula de llum** (drecera :kbd:`Ctrl"
"+L`) des del menú contextual. La selecció s'afegirà a la taula de llum i "
"s'obrirà la finestra separada. Quan torneu a la finestra principal del "
"digiKam, podreu accedir ràpidament a la taula de llum amb l'element de menú "
"«:menuselection:`Eines --> Taula de llum`» (tecla :kbd:`L`)."

#: ../../light_table/lighttable_overview.rst:39
msgid "Screencast Of The digiKam Light Table Item Selection From the Thumbbar"
msgstr ""
"Vídeo d'una captura de pantalla de la selecció d'elements per a la Taula de "
"llum del digiKam des de la barra de miniatures"

#: ../../light_table/lighttable_overview.rst:42
msgid "Playing With The Stack Contents"
msgstr "Jugar amb el contingut de la pila"

#: ../../light_table/lighttable_overview.rst:45
msgid "Synchronized Mode"
msgstr "Mode sincronitzat"

#: ../../light_table/lighttable_overview.rst:47
msgid ""
"From the thumbbar drag & drop images to the left and right comparison pane "
"below. A little arrow will indicate which copy is shown in which pane. The "
"current selected pane will be highlighted by frame. If you choose "
"**Synchronize** from the toolbar, any zoom and panning in one window will be "
"synchronously executed in the other pane, so that you can compare the same "
"areas of two images."
msgstr ""
"Des de la barra de miniatures, arrossegueu i deixeu anar imatges a la "
"subfinestra de comparació esquerra i dreta a continuació. Una petita fletxa "
"indicarà quina còpia es mostra a quin plafó. El plafó seleccionat actual es "
"ressaltarà per marc. Si escolliu **Sincronitza** des de la barra d'eines, "
"qualsevol zoom i moviment panoràmic en una finestra s'executarà de forma "
"sincrònica a l'altre plafó, de manera que pugueu comparar les mateixes àrees "
"de les dues imatges."

#: ../../light_table/lighttable_overview.rst:54
msgid ""
"Screencast Of The digiKam Light Table Using The **Synchonize** Mode With "
"Side By Side Views"
msgstr ""
"Vídeo d'una captura de pantalla de la Taula de llum del digiKam usant el "
"mode **Sincronització** amb les vistes costat a costat"

#: ../../light_table/lighttable_overview.rst:57
msgid "By Pair Mode"
msgstr "Mode per parelles"

#: ../../light_table/lighttable_overview.rst:59
msgid ""
"Another mode is better suited for quickly culling from a series of images. "
"If you choose **By Pair** from the toolbar, the first two images will be "
"automatically inserted into the comparison panes. Click on any thumbnail to "
"make it the left side preview, the adjacent thumbnail to the right will be "
"inserted into the right pane. That make it easy to sift through a series of "
"similar images."
msgstr ""
"Un altre mode és més adequat per a la selecció ràpida d'una sèrie d'imatges. "
"Si escolliu **Per parella** des de la barra d'eines, les dues primeres "
"imatges s'inseriran automàticament a dins de les subfinestres de comparació. "
"Feu clic a sobre de qualsevol miniatura per a crear la vista prèvia del "
"costat esquerre, la miniatura adjacent a la dreta s'inserirà a dins del "
"plafó dret. Això facilita examinar una sèrie d'imatges similars."

#: ../../light_table/lighttable_overview.rst:66
msgid ""
"Screencast of the digiKam Light Table Using The **By-Pair** Mode With Side "
"By Side Views"
msgstr ""
"Vídeo d'una captura de pantalla de la Taula de llum del digiKam usant el "
"mode **Per parelles** amb les vistes costat a costat"

#: ../../light_table/lighttable_overview.rst:68
msgid ""
"Of course, the usual edit actions work from the light table directly using :"
"menuselection:`File --> Edit...` :kbd:`F4`. This open current selected "
"preview canvas in **Image Editor**."
msgstr ""
"Per descomptat, les accions d'edició habituals funcionen directament des de "
"la taula de llum utilitzant l'element de menú «:menuselection:`Fitxer --> "
"Edita...`» (tecla :kbd:`F4`). Aquest llenç de vista prèvia seleccionat obert "
"a l'**editor d'imatges**."

#: ../../light_table/lighttable_overview.rst:70
msgid ""
"All image information from the main window right sidebar is available for "
"each of the two previews in the light table. The Light Table **Left "
"Sidebar** is dedicated to the **Left Pane**, and the **Right Sidebar** for "
"the **Right Pane**. This makes it easy to link visual differences to "
"exposure data for example."
msgstr ""
"Tota la informació de la imatge des de la barra lateral dreta de la finestra "
"principal està disponible per a cadascuna de les dues vistes prèvies a la "
"taula de llum. La **barra lateral esquerra** de la taula de llum està "
"dedicada a la **subfinestra esquerra** i la **barra lateral dreta** a la "
"**subfinestra dreta**. Això facilitarà la vinculació de les diferències "
"visuals amb les dades d'exposició, per exemple."

#: ../../light_table/lighttable_overview.rst:72
msgid ""
"In the lower right corner of each pane there you find a built-in panning "
"action (crossed arrows). Click on it and keep the left mouse button pressed "
"to pan across the image (with **Synchrone** mode, both images will show the "
"same viewing point)."
msgstr ""
"A la cantonada inferior dreta de cada subfinestra, trobareu una acció "
"panoràmica integrada (fletxes creuades). Feu-hi clic i manteniu premut el "
"botó esquerre del ratolí per a desplaçar-vos per la imatge (amb el mode "
"**Sincrònic**, ambdues imatges mostraran el mateix punt de visualització)."

#: ../../light_table/lighttable_overview.rst:74
msgid ""
"Zooming works the same as in other views: use indifferently the zoom slider "
"below the panes or **Ctrl-scroll wheel** to zoom in and out, with both "
"images when **Synchrone** mode is selected."
msgstr ""
"El zoom funciona igual que en les altres vistes: utilitzeu indistintament el "
"control lliscant de zoom sota les subfinestres o la drecera :kbd:`Ctrl-roda "
"de desplaçament` per a apropar i allunyar, amb les dues imatges quan se "
"selecciona el mode **Sincrònic**."

#: ../../light_table/lighttable_overview.rst:78
msgid ""
"If you use muti-screen on your computer, it's a good idea to place **Main "
"Window** on a screen and the **Light Table** on other one to increase your "
"experience."
msgstr ""
"Si utilitzeu múltiples pantalles a l'ordinador, és una bona idea posar la "
"**finestra principal** en una pantalla i la **Taula de llum** en una altra "
"per a augmentar la vostra experiència."

#: ../../light_table/lighttable_overview.rst:82
msgid ""
"Light Table has a specific configuration page from digiKam setup dialog. For "
"more details, read :ref:`this section <lighttable_settings>` from the manual."
msgstr ""
"La taula de llum té una pàgina de configuració específica en el diàleg de "
"configuració del digiKam. Per a més detalls, llegiu :ref:`aquesta secció "
"<lighttable_settings>` del manual."
