# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-04 00:39+0000\n"
"PO-Revision-Date: 2023-05-04 06:32+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../setup_application/metadata_settings.rst:1
msgid "digiKam Metadata Settings"
msgstr "Параметри метаданих digiKam"

#: ../../setup_application/metadata_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий"

#: ../../setup_application/metadata_settings.rst:14
msgid "Metadata Settings"
msgstr "Параметри метаданих"

#: ../../setup_application/metadata_settings.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../setup_application/metadata_settings.rst:18
msgid ""
"Image files can have some metadata embedded into the image file format. "
"These metadata can be stored in a number of standard formats as JPEG, TIFF, "
"PNG, JPEG2000, PGF, and RAW files. Metadata can be read and written in the "
"`Exif <https://en.wikipedia.org/wiki/Exif>`_, `IPTC <https://en.wikipedia."
"org/wiki/IPTC_Information_Interchange_Model>`_, and `XMP <https://en."
"wikipedia.org/wiki/Extensible_Metadata_Platform>`_ formats if they are "
"present in the file."
msgstr ""
"У файл зображення, записаний у певному форматі, може бути вбудовано "
"метадані. Ці метадані може бути збережено у багатьох стандартних форматах, "
"зокрема JPEG, TIFF, PNG, JPEG2000, PGF, та файлах цифрових негативів (RAW). "
"Якщо такі метадані є у файлі, їх може бути прочитано і записано у форматах "
"`Exif <https://en.wikipedia.org/wiki/Exif>`_, `IPTC <https://en.wikipedia."
"org/wiki/IPTC_Information_Interchange_Model>`_ і `XMP <https://en.wikipedia."
"org/wiki/Extensible_Metadata_Platform>`_."

#: ../../setup_application/metadata_settings.rst:23
msgid "Behavior Settings"
msgstr "Параметри поведінки"

#: ../../setup_application/metadata_settings.rst:25
msgid ""
"The **Behavior** tab allows you to select what information digiKam will "
"write to the metadata and control how digiKam will deal with this embedded "
"information."
msgstr ""
"За допомогою вкладки **Поведінка** ви можете вказати, які дані digiKam "
"записуватиме до метаданих у базі даних програми, і визначити поводження "
"digiKam з цими вбудованими даними."

#: ../../setup_application/metadata_settings.rst:31
msgid "The digiKam Metadata General Behavior Settings Page"
msgstr "Сторінка параметрів загальної поведінки щодо метаданих у digiKam"

#: ../../setup_application/metadata_settings.rst:35
msgid ""
"If the **Lazy Synchronization** option is turned on, digiKam will only write "
"metadata when user clicks on the **Apply Pending Changes To Metadata** icon "
"in the status bar or when application is shutdown."
msgstr ""
"Якщо увімкнено «ліниву» синхронізацію, digiKam записуватиме метадані лише "
"тоді, коли користувач натисне піктограму **Застосувати зміни з черги до "
"метаданих** на смужці стану або завершить роботу програми."

#: ../../setup_application/metadata_settings.rst:40
msgid "Sidecars Settings"
msgstr "Параметри додаткових файлів"

#: ../../setup_application/metadata_settings.rst:42
msgid ""
"The **Sidecars** tab allows user to control whether digiKam will read and "
"write from/to XMP sidecars or not. You can also customize the granularity of "
"write operations to XMP sidecar:"
msgstr ""
"За допомогою вкладки додаткових файлів користувач може визначити, чи буде "
"digiKam читати дані із додаткових файлів XMP або записувати до цих файлів "
"дані. Ви також можете налаштувати дії із запису до додаткового файла XMP:"

#: ../../setup_application/metadata_settings.rst:44
msgid "**Write to XMP sidecar only** will not touch the item metadata."
msgstr ""
"**Записувати лише до додатка XMP** не призведе до зміни метаданих самого "
"запису."

#: ../../setup_application/metadata_settings.rst:45
msgid ""
"**Write to item and XMP Sidecar** will touch both item and sidecar at the "
"same time."
msgstr ""
"**Записувати до зображення та до додатка XMP** призведе до запису одразу до "
"метаданих запису і додаткового файла."

#: ../../setup_application/metadata_settings.rst:46
msgid ""
"**Write to XMP sidecar for read-only item only** will handle sidecar for non-"
"writable items only, as video or RAW files for example."
msgstr ""
"**Записувати до додатка XMP лише для записів, призначених лише для читання** "
"призведе до створення додаткових файлів лише для непридатних до запису "
"елементів, зокрема файлів RAW та відео."

#: ../../setup_application/metadata_settings.rst:48
msgid ""
"Filename for the sidecars set to :file:`filename.ext.xmp`. For example, :"
"file:`image1.dng` will have a sidecar file named :file:`image1.dng.xmp`. "
"With the option **Sidecar file names are compatible with commercial "
"programs** digiKam will create the XMP sidecar files with a compatible file "
"name (:file:`image1.xmp`) used by many commercial programs."
msgstr ""
"Форматом назви файла для додаткових файлів є :file:`назва файла.суфікс.xmp`. "
"Наприклад, у файла :file:`зображення1.dng` буде додатковий файл із назвою :"
"file:`зображення1.dng.xmp`. У режимі **Назви файлів додатків сумісні із "
"комерційними програмами** digiKam створюватиме файли додатків XMP з "
"сумісними назвами файлів (:file:`зображення1.xmp`), які використовуються у "
"багатьох комерційних програмах."

#: ../../setup_application/metadata_settings.rst:54
msgid "The digiKam Metadata Sidecar Behavior Settings Page"
msgstr ""
"Сторінка параметрів поведінки щодо допоміжних файлів метаданих у digiKam"

#: ../../setup_application/metadata_settings.rst:58
msgid ""
"If the box **Read from sidecar files** is checked, digiKam will only read "
"the sidecar while ignoring the embedded metadata."
msgstr ""
"Якщо позначено пункт **Прочитати з додаткових файлів**, digiKam читатиме "
"лише файли додатків, ігноруючи вбудовані метадані."

#: ../../setup_application/metadata_settings.rst:60
msgid ""
"The option **Additional sidecar file extensions** allows to add extra "
"filename extensions to be processed alongside regular items, independently "
"of the XMP sidecars. These files will be hidden, but regarded as an "
"extension of the main file. Just write :file:`thm pp3` to support :file:"
"`filename.thm` (extra Jpeg thumbnail for RAW) and :file:`filename.pp3` "
"(RAWTheraPee metadata) sidecars."
msgstr ""
"За допомогою пункту **Суфікси назв додаткових файлів** можна додати "
"додаткові суфікси назв файлів, які буде оброблено разом із звичайними "
"записами, окремо від додаткових файлів XMP. Ці файли приховано, але вони "
"вважатимуться розширенням основного файла. Просто впишіть :file:`thm pp3`, "
"щоб забезпечити підтримку додаткових файлів :file:`назва_файла.thm` "
"(додаткова мініатюра Jpeg для RAW) і :file:`назва_файла.pp3` (метадані "
"RAWTheraPee)."

#: ../../setup_application/metadata_settings.rst:65
msgid "Rotation Settings"
msgstr "Параметри обертання"

#: ../../setup_application/metadata_settings.rst:71
msgid "The digiKam Metadata Rotation Behavior Settings Page"
msgstr "Сторінка параметрів поведінки щодо обертання у digiKam"

#: ../../setup_application/metadata_settings.rst:73
msgid ""
"**Show images/thumbnails rotated according to orientation tag**: this will "
"use any orientation information that your camera has included in the Exif "
"information to automatically rotate your photographs so that they are the "
"correct way up when displayed. It will not actually rotate the image file, "
"only the display of the image on the screen. If you want to permanently "
"rotate the image on file, you can click with the right mouse button on the "
"thumbnail and select **Auto-rotate/flip according to Exif orientation**. The "
"image will then be rotated on disk and the tag will be reset to \"normal\". "
"If your camera routinely gets this orientation information wrong you might "
"like to switch this feature off."
msgstr ""
"**Показувати зображення/мініатюри обернутими, відповідно до мітки "
"орієнтації**: якщо буде позначено цей пункт, програма використає всі дані "
"щодо орієнтації, які було записано вашим фотоапаратом до даних Exif, щоб "
"автоматично повернути ваші фотографії так, щоб їх було показано належним "
"чином. Програма не виконуватиме повороту зображення у файлі, обертання буде "
"здійснено лише під час показу на екрані. Якщо ви бажаєте обернути зображення "
"у файлі, ви можете навести вказівник миші на мініатюру зображення, клацнути "
"правою кнопкою миші і вибрати у контекстному меню пункт **Автоматичний "
"поворот або віддзеркалення за даними Exif**. У відповідь програма виконає "
"поворот зображення на диску і встановить значення мітки у орієнтації "
"«нормальна». Якщо фотоапарат надає неправильні відомості щодо орієнтації, "
"вам слід зняти позначку з цього пункту."

#: ../../setup_application/metadata_settings.rst:75
msgid ""
"**Set orientation tag to normal after rotate/flip**: the auto-rotate option "
"automatically corrects the orientation of images taken with digital cameras "
"that have an orientation sensor. The camera adds an orientation tag to the "
"image's Exif metadata. digiKam can read this tag to adjust the image "
"accordingly. If you manually rotate an image, these metadata will be "
"incorrect. This option will set the orientation tag to *Normal* after an "
"adjustment, assuming that you rotated it to the correct orientation. Switch "
"this off if you don't want digiKam to make changes to the orientation tag, "
"when you rotate or flip the image."
msgstr ""
"**Після обертання/віддзеркалення зображення змінити мітку орієнтації на "
"нормальну**: позначення пункту автоматичного обертання призведе до "
"автоматичного виправлення орієнтації зображень, знятих цифровими "
"фотоапаратами з датчиком орієнтації. Такі фотоапарати додають мітку "
"орієнтації до метаданих Exif метаданих. Програма digiKam може читати дані з "
"цієї мітки і враховувати значення під час показу зображення. Якщо ви "
"виконаєте обертання зображення вручну, значення цієї мітки вже не "
"відповідатиме куту повороту зображення. За допомогою цього пункту можна "
"встановити для мітки орієнтації, після виконання належного повороту, "
"значення «нормальна». Зніміть позначку з цього пункту, якщо ви не бажаєте, "
"щоб програма digiKam вносила зміни до мітки орієнтації після повороту або "
"віддзеркалення зображення."

#: ../../setup_application/metadata_settings.rst:80
msgid "Views Settings"
msgstr "Параметри панелей перегляду"

#: ../../setup_application/metadata_settings.rst:82
msgid ""
"These settings allows to customize the metadata contents displayed in Exif, "
"Makernotes, IPTC, XMP, and ExifTool viewers from the right sidebar. For more "
"details see :ref:`this section <metadata_view>` from the manual."
msgstr ""
"Ці параметри надають змогу налаштувати метадані, які показано у засобах "
"перегляду Exif, Makernotes, IPTC, XMP і ExifTool на правій бічній панелі. "
"Щоб дізнатися більше, зверніться до :ref:`цього розділу <metadata_view>` "
"підручник."

#: ../../setup_application/metadata_settings.rst:88
msgid "The digiKam Settings For The Metadata Viewers"
msgstr "Параметри digiKam для панелей перегляду метаданих"

#: ../../setup_application/metadata_settings.rst:93
msgid "ExifTool Settings"
msgstr "Параметри ExifTool"

#: ../../setup_application/metadata_settings.rst:95
msgid ""
"ExifTool is a backend engine that digiKam can use to process operations on "
"metadata, as view, read, and write. This panel only show the detection of "
"the ExifTool binary program, and the supported formats with the respective "
"read and write features."
msgstr ""
"ExifTool — рушій модуля обробки даних, яким digiKam користується для дій з "
"обробки метаданих, зокрема перегляду, читання та запису. На цій панелі буде "
"показано лише результати виявлення виконуваної програми ExifTool та список "
"підтримуваних форматів із відповідними можливостями читання і запису."

#: ../../setup_application/metadata_settings.rst:101
msgid "The digiKam Settings For The ExifTool Backend"
msgstr "Параметри digiKam для модуля обробки ExifTool"

#: ../../setup_application/metadata_settings.rst:105
msgid ""
"To replace the Exiv2 backend by ExifTool with all read and write metadata "
"operations, see the :ref:`Metadata Behavior <metadata_behavior>` section of "
"this manual."
msgstr ""
"Щоб замінити модуль обробки Exiv2 на ExifTool з усіма діями з читання та "
"запису метаданих, скористайтеся настановами з розділу :ref:`поведінки щодо "
"метаданих <metadata_behavior>` цього підручника."

#: ../../setup_application/metadata_settings.rst:110
msgid "Baloo Settings"
msgstr "Параметри Baloo"

#: ../../setup_application/metadata_settings.rst:112
msgid ""
"**Baloo** is the file indexing and file search framework for **KDE Plasma** "
"under Linux, with a focus on providing a very small memory footprint along "
"with an extremely fast searching. Baloo is not an application, but a daemon "
"to index files."
msgstr ""
"**Baloo** — комплект бібліотек для індексування та пошуку файлів для "
"**Плазми KDE** у Linux із акцентом на зменшеному споживанні пам'яті та "
"надзвичайно швидкому пошуку даних. Baloo не є користувацькою програмою, а є "
"фоновою службою для індексування файлів."

#: ../../setup_application/metadata_settings.rst:118
msgid "The digiKam Settings For The Baloo Metadata Search Engine"
msgstr "Параметри digiKam для рушія пошуку метаданих Balooe"

#: ../../setup_application/metadata_settings.rst:120
msgid ""
"This page allows to share metadata stored in digiKam database with the Baloo "
"search engine. Extra applications as **KDE Dolphin** file manager can use "
"the Baloo interface to provide file search results with items managed by the "
"digiKam database."
msgstr ""
"За допомогою цієї сторінки можна використатися метадані, що зберігаються у "
"базі даних digiKam у рушії пошуку Baloo. Зовнішні програми, зокрема програма "
"для керування файлами **KDE Dolphin**, зможуть скористатися інтерфейсом "
"Baloo для надання результатів пошуку для записів, якими керує база даних "
"digiKam."

#: ../../setup_application/metadata_settings.rst:124
msgid ""
"This page is only available under Linux, not Windows and macOS. The KDE "
"Plasma **Files Indexer** feature must be enabled in the KDE Plasma control "
"Panel."
msgstr ""
"Цією сторінкою можна скористатися лише у Linux, а не у Windows і macOS. Щоб "
"скористатися нею, слід увімкнути можливість **індексування файлів** на "
"панелі керування Плазмою KDE."

#: ../../setup_application/metadata_settings.rst:129
msgid "Advanced Settings"
msgstr "Додаткові параметри"

#: ../../setup_application/metadata_settings.rst:131
msgid ""
"The **Advanced** tab allows you to manage namespaces used by digiKam to "
"store and retrieve tags, ratings and comments. This functionality is often "
"used by advanced users to synchronize metadata between different software. "
"Please leave the default settings if you are not sure what to do here."
msgstr ""
"За допомогою вкладки додаткових параметрів можна керувати просторами назв, "
"які використовуватимуться digiKam для зберігання і отримання міток, оцінок і "
"коментарів. Ця функціональна можливість часто використовується досвідченими "
"користувачами для синхронізації метаданих між різними програмами. Будь "
"ласка, не змінюйте типових значень параметрів на цій вкладці, якщо вам "
"достеменно не відоме їхнє призначення."

#: ../../setup_application/metadata_settings.rst:133
msgid "The categories that you can manage with these advanced settings are:"
msgstr ""
"Категорії, якими ви можете керувати за допомогою цих додаткових параметрів, "
"є такими:"

#: ../../setup_application/metadata_settings.rst:135
msgid ""
"**Caption**: all languages-alternative comments (supported by XMP only), "
"else the simple comments values (Exif and IPTC)."
msgstr ""
"**Підпис**: усі коментарі із перекладеними альтернативами (підтримку "
"передбачено лише у XMP), інакше прості значення коментарів (Exif і IPTC)."

#: ../../setup_application/metadata_settings.rst:136
msgid ""
"**Color Label**: the color labels properties to apply on items in your "
"workflow."
msgstr ""
"**Кольорова мітка**: властивості кольорових міток, які слід застосовувати до "
"записів у процесі обробки."

#: ../../setup_application/metadata_settings.rst:137
msgid "**Rating**: the stars properties to apply on items in your workflow."
msgstr ""
"**Оцінка**: властивості оцінок, які слід застосовувати до записів у процесі "
"обробки."

#: ../../setup_application/metadata_settings.rst:138
msgid ""
"**Tags**: the nested keywords hierarchy to apply on items in your workflow "
"(supported by XMP only), else the simple flat list of keywords (Exif and "
"IPTC)."
msgstr ""
"**Мітки**: розгалужена ієрархія ключових слів, які слід застосовувати до "
"записів у процесі обробки (підтримку передбачено лише для XMP), інакше "
"простий список ключових слів (Exif і IPTC)."

#: ../../setup_application/metadata_settings.rst:139
msgid ""
"**Title**: all languages-alternative titles (supported by XMP only), else "
"the simple title values (Exif and IPTC)."
msgstr ""
"**Заголовок**: усі заголовки із перекладеними альтернативами (підтримку "
"передбачено лише у XMP), інакше прості значення заголовка (Exif і IPTC)."

#: ../../setup_application/metadata_settings.rst:141
msgid ""
"For each category you can set the read and write behavior in metadata. The "
"default settings is to **Unify Read and Write** operations, but if you "
"disable this option, you can customize **Read Options** and **Write "
"Options** independently."
msgstr ""
"Для кожної категорії ви можете встановити поведінку щодо читання та запису "
"метаданих. Типовим параметрами є **Уніфікувати читання і запис**, але якщо "
"ви знімете позначку з цього пункту, ви можете налаштувати **Параметри "
"читання** і **Параметри запису** незалежно."

#: ../../setup_application/metadata_settings.rst:147
msgid "The digiKam Advanced Metadata Settings For the **Caption** Category"
msgstr "Розширені параметри метаданих digiKam для категорії **Підпис**"

#: ../../setup_application/metadata_settings.rst:149
msgid ""
"On this example, the top **Caption** entry in the list is **Xmp.dc."
"description**, and it will be read by digiKam first. If it contains a valid "
"value it will be used, otherwise the next entry named **Xmp.exif."
"UserComment**, etc. The entries list priority is high on the top and low on "
"the bottom. The entry in the list are used only if item is enabled with the "
"checkbox preceding the name."
msgstr ""
"У цьому прикладі верхнім записом **Підпис** у списку є **Xmp.dc."
"description**, і їх буде спочатку прочитано digiKam. Якщо там міститься "
"коректне значення, його буде використано, якщо ж це не так, буде використано "
"наступний запис із назвою **Xmp.exif.UserComment**, тощо. Пріоритетність "
"записів у списку зменшується з верхнього до нижнього записів. Запис у списку "
"буде використано, лише якщо пункт у списку позначено у полі перед його "
"назвою."

#: ../../setup_application/metadata_settings.rst:151
msgid "With the buttons on the right side, you can customize the list:"
msgstr ""
"За допомогою кнопок, які розташовано праворуч, ви можете налаштувати список:"

#: ../../setup_application/metadata_settings.rst:153
msgid "**Add**: allows to add a new entry in the tags list."
msgstr "**Додати**: уможливлює додавання нового запису до списку міток."

#: ../../setup_application/metadata_settings.rst:154
msgid "**Edit**: allows to modify the current select entry in the list."
msgstr ""
"**Змінити**: надає змогу вносити зміни до поточного позначеного запису у "
"списку."

#: ../../setup_application/metadata_settings.rst:155
msgid "**Delete**: allows to remove the current select entry in the list."
msgstr "**Вилучити**: надає змогу вилучити поточний запис зі списку."

#: ../../setup_application/metadata_settings.rst:156
msgid ""
"**Move up**: allows to move the current select entry in the list to a higher "
"priority."
msgstr ""
"**Пересунути вище**: надає змогу пересунути поточний позначений запис у "
"списку на вищу позицію."

#: ../../setup_application/metadata_settings.rst:157
msgid ""
"**Move Down**: allows to move the current selected entry in the list to a "
"lower priority."
msgstr ""
"**Пересунути нижче**: надає змогу пересунути поточний позначений запис у "
"списку на нижчу позицію."

#: ../../setup_application/metadata_settings.rst:158
msgid "**Revert Changes**: allows to revert last changes done on the list."
msgstr "**Скасувати зміни**: надає змогу скасувати останні зміни у списку."

#: ../../setup_application/metadata_settings.rst:159
msgid ""
"**Save Profile**: allows to save the current configuration to a **Profile** "
"file."
msgstr ""
"**Зберегти профіль**: надає змогу зберегти поточні налаштування до файла "
"**профілю**."

#: ../../setup_application/metadata_settings.rst:160
msgid ""
"**Load Profile**: allows to load a saved configuration from a **Profile** "
"file."
msgstr ""
"**Завантажити профіль**: надає змогу завантажити збережені налаштування з "
"файла **профілю**."

#: ../../setup_application/metadata_settings.rst:161
msgid ""
"**Revert To Default**: allows to reset the current list to the default "
"values."
msgstr ""
"**Повернутися до типового**: надає змогу відновити типові значення у "
"поточному списку."

#: ../../setup_application/metadata_settings.rst:163
msgid ""
"The **Profile** are simple ini-based text file used to store the advanced "
"metadata settings to the disk. A profile can be loaded to overload the "
"current configuration, depending of your workflow and the rules to apply for "
"the best interoperability with other photo management programs. digiKam "
"comes with a compatibility profile for **DarkTable**."
msgstr ""
"**Профіль** є простим текстовим файлом на основі формату ini для зберігання "
"розширених параметрів метаданих на диск. Профіль може бути завантажено для "
"перезавантаження поточних налаштувань, залежно від вашого робочого процесу і "
"правил, які слід застосувати для покращення взаємодії із іншими програмами "
"для керування фотографіями. digiKam постачається із профілем сумісності із "
"**DarkTable**."

#: ../../setup_application/metadata_settings.rst:167
msgid ""
"We recommend to always put XMP tags to the top priority on this list, as XMP "
"has better features than IPC and Exif."
msgstr ""
"Рекомендуємо завжди пересувати мітки XMP на вершину списку, оскільки "
"можливості XMP є ширшими, ніж у IPC і Exif."

#: ../../setup_application/metadata_settings.rst:171
msgid ""
"The **Tags** category provide an extra option named **Read All Metadata For "
"Tags** to force operations on all the namespaces."
msgstr ""
"У категорії **Мітки** передбачено додатковий пункт із назвою **читати усі "
"метадані для міток** для примусового виконання дій над усіма просторами назв."
