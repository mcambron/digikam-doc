# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-02-25 11:18+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../import_tools/remote_import.rst:1
msgid "digiKam Import from Remote Computer"
msgstr "Importeren van computer op afstand van digiKam"

#: ../../import_tools/remote_import.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, remote, computer, import"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, op afstand, computer, importeren"

#: ../../import_tools/remote_import.rst:14
msgid "Import from Remote Computer"
msgstr "Importeren van computer op afstand"

#: ../../import_tools/remote_import.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../import_tools/remote_import.rst:18
msgid ""
"This tool allows to download files from a remote computer to your "
"collections using network. It available from the :menuselection:`Import --> "
"Import from remote storage` :kbd:`Alt+Shift+K` menu entry or the "
"corresponding icon from the **Tools** tab in Right Sidebar."
msgstr ""
"Dit hulpmiddel biedt het downloaden van bestanden uit een computer op "
"afstand naar uw verzamelingen naar via uw netwerk. Het is beschikbaar via "
"het menu-item :menuselection:`Importeren --> Uit opslag op afstand "
"importeren` :kbd:`Ctrl+Alt+Shift+K` of het bijbehorende pictogram uit het "
"tabblad **Hulpmiddelen** in de rechter zijbalk."

#: ../../import_tools/remote_import.rst:24
msgid "The Import from Remote Computer Dialog"
msgstr "De dialoog van importeren van computer op afstand"

#: ../../import_tools/remote_import.rst:26
msgid ""
"To choose files to import press **Select Import Location**, a native desktop "
"urls selector as below must appear where you can see the discovered remote "
"devices selecting the network section:"
msgstr ""
"Om te importeren bestanden te kiezen druk op **Importlocatie selecteren**, "
"een inheemse URL-kiezer zoals onderstaand moet verschijnen waar u de "
"ontdekte apparaten op afstand kan zien door de netwerksectie te selecteren:"

#: ../../import_tools/remote_import.rst:32
msgid ""
"The Urls Selector Dialog Under Linux Choosing Files from a NAS Sharing "
"Contents trough Samba File System."
msgstr ""
"De dialoog van de URL-kiezer onder Linux voor het kiezen van bestanden uit "
"een NAS voor het delen van inhoud via het bestandssysteem Samba."

#: ../../import_tools/remote_import.rst:34
msgid ""
"In this dialog, you can use the protocols as **fish** (ssh), **ftp**, "
"**smb**, etc, to be connected to the remote computer. For example,"
msgstr ""
"In deze dialoog kunt u protocollen als **fish** (ssh), **ftp**, **smb**, "
"etc., gebruiken om verbonden te worden met de computer op afstand. "
"Bijvoorbeeld:"

#: ../../import_tools/remote_import.rst:36
msgid ""
"**fish://user_name@remote_computer**: connect to the *remote_computer* with "
"*user_name* using SSH protocol (Secure SHell)."
msgstr ""
"**fish://user_name@remote_computer**: verbindt met de *remote_computer* met "
"*user_name* met gebruik van het SSH-protocol (Secure SHell)."

#: ../../import_tools/remote_import.rst:37
msgid ""
"**ftp://user_name@remote_computer**: connect to the *remote_computer* with "
"*user_name* using FTP protocol (File Transfer Protocol)."
msgstr ""
"**ftp://gebruikersnaam@computer_op_afstand**: verbinden met de "
"*computer_op_afstand* met *gebruikersnaam* met FTP-protocol (File Transfer "
"Protocol)."

#: ../../import_tools/remote_import.rst:41
msgid ""
"The native url selector dialog is only available if you turn on the right "
"option from the **Misc/Appearance** section of the :ref:`configuration panel "
"<appearance_settings>`."
msgstr ""
"De inheemse dialoog voor URL-kiezer is alleen beschikbaar als u de juiste "
"optie uit de sectie **Div/uiterlijk** van het :ref:`configuratiepaneel "
"<appearance_settings>` aanzet."

#: ../../import_tools/remote_import.rst:43
msgid ""
"After selecting the files to import and closing the native desktop urls "
"selector, these ones must appear to main dialog in the list below **Select "
"import location** button."
msgstr ""
"Na selectie van de te importeren bestanden en de inheemse bureaublad-url-"
"kiezer, moeten deze in de hoofddialoog verschijnen in de lijst onder de knop "
"**Importlocatie selecteren**."

#: ../../import_tools/remote_import.rst:45
msgid ""
"Below this list of files, the main dialog propose the hierarchy of physical "
"**Albums** from your collection. Selected one target entry to import files, "
"or if you want a new one, just press **New Album** button to create a nested "
"entry in the tree-view."
msgstr ""
"Onder deze lijst met bestanden biedt de hoofddialoog de hiërarchie van de "
"fysieke **Albums** uit uw verzameling. Selecteer één doelitem om de "
"bestanden naar te importeren of als u wilt een nieuwe, druk gewoon op de "
"knop **Nieuw album** om een genest item in de boomstructuurweergave aan te "
"maken."

#: ../../import_tools/remote_import.rst:47
msgid ""
"When you select right target album to download files, press **Start Import** "
"button to process files. You can abort operation pressing **Close** button."
msgstr ""
"Wanneer u het rechter doelalbum selecteert om bestanden te downloaden, druk "
"op de knop **Importeren starten** om bestanden te verwerken. U kunt "
"bewerking stoppen door knop **Annuleren** in te drukken."

#~ msgid "The Import from Remote Computer Url Selector Dialog Under Linux."
#~ msgstr ""
#~ "De dialoog van de URL-kiezer voor importeren van computer op afstand "
#~ "onder Linux."

#~ msgid ""
#~ "By default, the tool proposes to export the currently selected items from "
#~ "the icon-view. The **+** Photos button can be used to append more items "
#~ "on the list."
#~ msgstr ""
#~ "Standaard zal het hulpmiddel voorstellen om de nu geselecteerde items uit "
#~ "de pictogramweergave te exporteren. De knop **+** foto's kan gebruikt "
#~ "worden om meer items aan de lijst toe te voegen."
